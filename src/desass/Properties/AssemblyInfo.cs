﻿using desass;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("desass")]
[assembly: AssemblyDescription("Desktop Assistant")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("desass")]
[assembly: AssemblyCopyright("Copyright © Michał Rakoczy 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("2817f3e7-82b3-4d0f-9375-d1cb623d4c70")]

[assembly: AssemblyVersion(Constants.Version)]
[assembly: AssemblyFileVersion(Constants.Version)]
