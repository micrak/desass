﻿namespace desass
{
    using System;
    using System.Windows.Forms;

    public class TrayIconWrapper : IDisposable
    {
        private readonly uint mainThreadId;

        private readonly NotifyIcon notifyIcon;

        public TrayIconWrapper(uint mainThreadid)
        {
            this.mainThreadId = mainThreadid;

            var exitMenuItem = new MenuItem();
            exitMenuItem.Text = "Exit";
            exitMenuItem.Click += this.ExitMenuItemOnClick;

            var contextMenu = new ContextMenu();
            contextMenu.MenuItems.Add(exitMenuItem);

            this.notifyIcon = new NotifyIcon();
            this.notifyIcon.Icon = Resources.Icon;
            this.notifyIcon.ContextMenu = contextMenu;
        }        

        public event EventHandler ExitButtonClicked;

        public void Dispose()
        {
            this.notifyIcon.Dispose();
        }

        public void Show()
        {            
            this.notifyIcon.Visible = true;            
        }

        private void ExitMenuItemOnClick(object sender, EventArgs e)
        {
            this.ExitButtonClicked?.Invoke(this, EventArgs.Empty);
        }
    }
}
