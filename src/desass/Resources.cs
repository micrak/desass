﻿namespace desass
{
    using System.Drawing;
    using System.IO;

    public static class Resources
    {
        public static readonly Icon Icon = new Icon(GetResource("desass.icon.ico"));

        private static Stream GetResource(string name)
        {
            return typeof(Resources).Assembly.GetManifestResourceStream(name);
        }
    }
}
