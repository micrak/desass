﻿namespace desass
{
    using desass.Win32;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class WindowsIndex
    {
        private readonly List<IndexedWindow> indexedWindows = new List<IndexedWindow>();

        public List<IndexedWindow> Find(string searchString)
        {
            var result = this.GetAllVisible()
                .Where(g => ContainsText(g, searchString))
                .ToList();

            return result;
        }

        private bool ContainsText(IndexedWindow g, string searchString)
        {
            if (this.MatchesFullText(g, searchString))
            {
                return true;
            }

            if (this.MatchesAnyKeyword(g, searchString))
            {
                return true;
            }

            if (this.MatchesKeywordStartLetters(g, searchString))
            {
                return true;
            }

            return false;
        }

        private bool MatchesFullText(IndexedWindow g, string searchString)
        {
            if (g.Title != null && g.Title.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) > -1)
            {
                return true;
            }

            if (g.ModuleName != null && g.ModuleName.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) > -1)
            {
                return true;
            }

            return false;
        }

        private bool MatchesAnyKeyword(IndexedWindow g, string searchString)
        {
            foreach (var keyword in g.Keywords)
            {
                if (keyword.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) > -1)
                {
                    return true;
                }
            }

            return false;
        }

        private bool MatchesKeywordStartLetters(IndexedWindow g, string searchString)
        {
            var keywords = g.Keywords.ToList();

            int foundKeywords = 0;

            for (int i = 0; i < searchString.Length; i++)
            {
                char c = char.ToLowerInvariant(searchString[i]);

                var keyword = keywords.FirstOrDefault(k => char.ToLowerInvariant(k[0]) == c);
                if (keyword != null)
                {
                    foundKeywords++;
                    keywords.Remove(keyword);
                    continue;
                }
            }

            var result = foundKeywords == searchString.Length;

            return result;
        }

        public List<IndexedWindow> GetAllVisible()
        {
            return this.indexedWindows.Where(g => g.IsVisible)
                .OrderBy(g => g.ModuleName)
                .ThenBy(g => g.Title)
                .ToList();
        }

        public void Rebuild()
        {
            this.indexedWindows.Clear();

            var processes = NativeMethods.GetProcessesSnapshots();

            StringBuilder buffer = new StringBuilder(2048);
            var bufferLength = new IntPtr(buffer.Capacity);

            NativeMethods.EnumWindows(new EnumWindowsProc((handle, param) =>
            {
                if (!NativeMethods.IsWindowVisible(handle))
                {
                    return true;
                }

                NativeMethods.SendMessage(handle, WM.GETTEXT, bufferLength, buffer);

                if (buffer.Length == 0)
                {
                    return true;
                }

                uint processId;
                NativeMethods.GetWindowThreadProcessId(handle, out processId);

                PROCESSENTRY32 entry;
                if (!processes.TryGetValue((int)processId, out entry))
                {
                    return true;
                }

                var window = new IndexedWindow();
                window.Handle = handle;
                window.Title = buffer.ToString();
                window.ProcessId = processId;
                window.IsVisible = true;
                window.ModuleName = entry.szExeFile;
                window.RebuildKeywords();

                this.indexedWindows.Add(window);

                return true;
            }), IntPtr.Zero);
        }
    }
}
