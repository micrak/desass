﻿namespace desass
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class IndexedWindow
    {
        private readonly List<string> keywords = new List<string>();

        public IntPtr Handle { get; set; }

        public string Title { get; set; }

        public uint ProcessId { get; set; }

        public string ModuleName { get; set; }

        public bool IsVisible { get; set; }

        public IEnumerable<string> Keywords => this.keywords;

        public void RebuildKeywords()
        {
            this.keywords.Clear();

            var values = this.AnalyseString(this.ModuleName)
                            .Concat(this.AnalyseString(this.Title))
                            .Distinct();

            this.keywords.AddRange(values);
        }

        private List<string> AnalyseString(string value)
        {
            var buffer = new StringBuilder();

            for (int i = 0; i < value.Length; i++)
            {
                char c = value[i];

                if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
                {
                    if (i > 0)
                    {
                        char prev = value[i - 1];

                        if (char.IsLower(prev) && char.IsUpper(c))
                        {
                            buffer.Append(' ');
                        }
                    }

                    buffer.Append(char.ToLowerInvariant(c));
                }
                else
                {
                    buffer.Append(' ');
                }                
            }

            var result = buffer.ToString()
                .Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                .Where(g => g.Length > 1)
                .ToList();

            return result;
        }
    }
}
