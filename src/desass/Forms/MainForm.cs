﻿namespace desass.Forms
{
    using desass.Win32;
    using System;
    using System.Windows.Forms;

    public class MainForm : Form
    {
        private TrayIconWrapper trayIcon;

        private bool isSearchFormActive = false;

        public MainForm()
        {
            this.Opacity = 0;
            this.ShowInTaskbar = false;
            this.Width = 0;
            this.Height = 0;
            this.FormBorderStyle = FormBorderStyle.None;
            this.Shown += this.OnShown;
        }

        private void OnShown(object sender, EventArgs e)
        {
            this.RegisterHotKey();
            this.CreateTrayIcon();
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM.HOTKEY)
            {
                this.ShowSearchForm();
                return;
            }

            base.WndProc(ref m);
        }

        private void RegisterHotKey()
        {
            const int AnyId = 12345;
            const uint MOD_ALT = 0x0001;
            const uint VK_TILDE = 0xC0;

            var hkRes = NativeMethods.RegisterHotKey(this.Handle, AnyId, MOD_ALT, VK_TILDE);
            if (!hkRes)
            {
                throw new Exception("Failed to register hotkey");
            }
        }

        private void ShowSearchForm()
        {
            if (this.isSearchFormActive)
            {
                return;
            }

            try
            {
                this.isSearchFormActive = true;

                var windowsIndex = new WindowsIndex();
                windowsIndex.Rebuild();

                using (var form = new SearchForm(windowsIndex))
                {
                    form.ShowDialog();
                }
            }
            finally
            {
                this.isSearchFormActive = false;
            }
        }

        private void CreateTrayIcon()
        {
            this.trayIcon = new TrayIconWrapper(NativeMethods.GetCurrentThreadId());
            this.trayIcon.ExitButtonClicked += this.OnTrayIconExitButtonClicked;
            this.trayIcon.Show();
        }

        private void OnTrayIconExitButtonClicked(object sender, EventArgs e)
        {
            this.trayIcon.Dispose();
            this.Close();
        }
    }
}
