﻿namespace desass.Forms
{
    using desass.Win32;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;

    public partial class SearchForm : Form
    {
        private readonly List<IndexedWindow> windows = new List<IndexedWindow>();

        private readonly WindowsIndex windowsIndex;        

        public SearchForm(WindowsIndex windowsIndex)
        {
            this.windowsIndex = windowsIndex;

            this.InitializeComponent();

            this.PreviewKeyDown += this.ThisPreviewKeyDown;
            this.Deactivate += this.ThisOnDeactivate;
            this.Shown += this.ThisShown;

            this.Icon = Resources.Icon;
            this.ShowIcon = true;

            this.textBox.KeyDown += this.TextBoxKeyDown;
            this.textBox.PreviewKeyDown += this.TextBoxPreviewKeyDown;
            this.textBox.TextChanged += this.TextBoxOnTextChanged;

            this.listBox.PreviewKeyDown += this.ListBoxPreviewKeyDown;
            this.listBox.MouseDoubleClick += this.ListBoxMouseDoubleClick;

            this.labelInfo.Text = $"desass.exe {Constants.Version}";            

            this.Search(null);

            var position = Cursor.Position;
            var screen = Screen.FromPoint(position);
            var screenLocation = screen.WorkingArea.Location;
            var size = screen.WorkingArea.Size;
            this.Location = new Point(
                screenLocation.X + size.Width / 2 - this.Width / 2,
                screenLocation.Y + size.Height / 2 - this.Height / 2);
        }

        private void ListBoxPreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void ListBoxMouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.ShowSelectedAndClose();
        }

        private void ThisOnDeactivate(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ThisShown(object sender, EventArgs e)
        {
            this.Activate();
            this.Refresh();
        }

        private void TextBoxOnTextChanged(object sender, EventArgs e)
        {
            var searchText = this.textBox.Text;
            this.Search(searchText);
        }

        private void Search(string searchText)
        {
            searchText = searchText?.Trim() ?? string.Empty;

            this.windows.Clear();

            if (searchText.Length > 0)
            {
                this.windows.AddRange(this.windowsIndex.Find(searchText));
            }
            else
            {
                this.windows.AddRange(this.windowsIndex.GetAllVisible());
            }

            this.ReloadListBox();
        }

        private void ReloadListBox()
        {
            this.listBox.Items.Clear();

            if (this.windows.Count == 0)
            {
                this.listBox.Refresh();
                return;
            }

            foreach (var window in this.windows)
            {
                this.listBox.Items.Add($"{window.Title} [{window.ModuleName}]");
            }

            this.SelectItem(0);

            this.listBox.Refresh();
        }

        private void SelectItem(int index)
        {
            if (index < 0)
            {
                index = this.windows.Count - 1;
            }

            if (index > this.windows.Count - 1)
            {
                index = 0;
            }

            var window = this.windows.ElementAtOrDefault(index);
            if (window != null)
            {
                this.listBox.SelectedIndex = index;
            }
        }

        private void TextBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                e.Handled = true;
                this.SelectItem(this.listBox.SelectedIndex - 1);
            }

            if (e.KeyCode == Keys.Down)
            {
                e.Handled = true;
                this.SelectItem(this.listBox.SelectedIndex + 1);
            }
        }

        private void TextBoxPreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {           
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }

            if (e.KeyCode == Keys.Enter)
            {
                this.ShowSelectedAndClose();   
            }
        }

        private void ShowSelectedAndClose()
        {
            var selectedItem = this.windows.ElementAtOrDefault(this.listBox.SelectedIndex);
            if (selectedItem != null)
            {
                var handle = selectedItem.Handle;

                var styles = NativeMethods.GetWindowLongPtr(handle, (int)WindowLongFlags.GWL_STYLE);

                if ((styles & (int)WindowStyles.WS_MINIMIZE) > 0)
                {
                    NativeMethods.ShowWindow(handle, (int)ShowWindowCommands.Restore);
                }
                else
                {
                    NativeMethods.SetForegroundWindow(handle);
                }

                this.Close();
            }
        }

        private void ThisPreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
