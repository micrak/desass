﻿namespace desass
{
    using desass.Forms;
    using System.Windows.Forms;

    public static class Program
    {
        public static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.Run(new MainForm());
        }                      
    }
}
